-- file:	top.vhd
-- author:	CYRIL URBAN
-- date:	2015-05-01
-- brief:	The game LightsOut

library ieee;

use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.vga_controller_cfg.all;
use work.maskpack.ALL;

architecture main of tlv_pc_ifc is
	
	constant IDX_TOP    : NATURAL := 0;
	constant IDX_LEFT   : NATURAL := 1;
	constant IDX_RIGHT  : NATURAL := 2;
	constant IDX_BOTTOM : NATURAL := 3;
	constant IDX_ENTER  : NATURAL := 4; 

	signal vga_mode   : std_logic_vector(60 downto 0); -- default 640x480x60

	signal color 	  : std_logic_vector(8 downto 0);
	signal light 	  : std_logic_vector(8 downto 0) := "111111000"; -- YELLOW (active)
	signal  dark 	  : std_logic_vector(8 downto 0) := "100100100"; -- SHADOW (not active)
	signal  sel_light : std_logic_vector(8 downto 0) := "111000000"; -- RED (selected + active)
	signal  sel_dark  : std_logic_vector(8 downto 0) := "000111000"; -- GREEN (selected + not active)

	alias red   is color(8 downto 6);
	alias green is color(5 downto 3);
	alias blue  is color(2 downto 0);

	signal row 	  : std_logic_vector(11 downto 0);
	signal col    : std_logic_vector(11 downto 0);

	-- actual signals for display
	signal ACTIVE_CELL   : std_logic_vector(24 downto 0);
	signal SELECTED_CELL : std_logic_vector(24 downto 0);

	-- INIT (reset A)
	signal init_active   : std_logic_vector(24 downto 0) := "1101110001000001000111011";
	signal init_selected : std_logic_vector(24 downto 0) := "0000000000001000000000000";

	-- KEYBOARD SIGNALS
	signal kbrd_data_out : std_logic_vector(15 downto 0);
	signal keys: std_logic_vector(4 downto 0);
	--signal kbrd_data_vld : std_logic;

	-- REGISTERS
	signal sel_req : std_logic_vector(99 downto 0);
	signal inv_req : std_logic_vector(99 downto 0);

	signal RESET_CELL : STD_LOGIC := '1';

	signal en_1MHz,en_2Hz : std_logic;
begin

gen_1mhz: entity work.engen generic map ( MAXVALUE => 39) port map ( CLK => CLK, ENABLE => '1', EN => en_1MHz );
gen_2hz: entity work.engen generic map ( MAXVALUE => 160000) port map ( CLK => CLK, ENABLE => en_1MHz, EN => en_2Hz );

	-- PORT OF KEYBOARD
	kbrd_ctrl: entity work.keyboard_controller(arch_keyboard)
	port map (
		CLK => CLK,
		RST => RESET,

		DATA_OUT => kbrd_data_out(15 downto 0),
		
		KB_KIN   => KIN,
		KB_KOUT  => KOUT
	);

	riadky: for y in 0 to 4 generate
	begin  
		stlpce:  for x in 0 to 4 generate 
			begin
    		
   			CELL_inst: entity work.cell 
   				generic map (MASK => getmask(x,y,5,5))

   				port map (
				SELECTED => SELECTED_CELL(y*5+x),
				ACTIVE 	 => ACTIVE_CELL(y*5+x),

				INIT_ACTIVE   => init_active(y*5+x),
				INIT_SELECTED => init_selected(y*5+x),

				SELECT_REQ_OUT => sel_req((((y*5+x)*4 ) +3) downto ( (y*5+x)*4+0) ),
				INVERT_REQ_OUT => inv_req((y*5+x)*4+3 downto (y*5+x)*4+0),

				SELECT_REQ_IN(IDX_TOP)    => sel_req(  (((y-1) mod 5)*5 + x)*4 + IDX_BOTTOM  ),
				SELECT_REQ_IN(IDX_RIGHT)  => sel_req(  (y*5 + ((x+1) mod 5))*4 + IDX_LEFT    ),
				SELECT_REQ_IN(IDX_LEFT)   => sel_req(  (y*5 + ((x-1) mod 5))*4 + IDX_RIGHT   ),
				SELECT_REQ_IN(IDX_BOTTOM) => sel_req(  (((y+1) mod 5)*5 + x)*4 + IDX_TOP     ),

				INVERT_REQ_IN(IDX_TOP)    => inv_req(  (((y-1) mod 5)*5 + x)*4 + IDX_BOTTOM  ),
				INVERT_REQ_IN(IDX_RIGHT)  => inv_req(  (y*5 + ((x+1) mod 5))*4 + IDX_LEFT    ),
				INVERT_REQ_IN(IDX_LEFT)   => inv_req(  (y*5 + ((x-1) mod 5))*4 + IDX_RIGHT   ),
				INVERT_REQ_IN(IDX_BOTTOM) => inv_req(  (((y+1) mod 5)*5 + x)*4 + IDX_TOP     ),

				CLK   => CLK,
				RESET => RESET_CELL,
				KEYS  => keys(4 downto 0)
			);

		end generate stlpce;
	end generate riadky;

	-- Nastaveni grafickeho rezimu (640x480, 60 Hz refresh)
	setmode(r640x480x60, vga_mode);

	vga: entity work.vga_controller(arch_vga_controller) 
		generic map (REQ_DELAY => 1)
		port map ( 
			CLK    		=> CLK, 
			RST    		=> RESET,
			ENABLE 		=> '1',
			MODE   		=> vga_mode,

			DATA_RED    => red,
			DATA_GREEN  => green,
			DATA_BLUE   => blue,
			ADDR_COLUMN => col,
			ADDR_ROW    => row,

			VGA_RED     => RED_V,
			VGA_BLUE    => BLUE_V,
			VGA_GREEN 	=> GREEN_V,
			VGA_HSYNC 	=> HSYNC_V,
			VGA_VSYNC 	=> VSYNC_V
		);

	--=========================================================================
	-- KEY PRESS PROCESS
	klavesy: process(CLK)
	begin
		if rising_edge(CLK) then
		  keys <= "00000";
		  if (en_2Hz='1') then
			--RESET_CELL <= '0';
			
			if kbrd_data_out(1)='1' then     -- key 6
				RESET_CELL <= '0';
				keys(IDX_LEFT)<='1';
			elsif kbrd_data_out(9)='1' then  -- key 4
				RESET_CELL <= '0';
				keys(IDX_RIGHT)<='1';
			elsif kbrd_data_out(4)='1' then  -- key 2
				RESET_CELL <= '0';
				keys(IDX_TOP)<='1';
			elsif kbrd_data_out(6)='1' then  -- key 8
				RESET_CELL <= '0';
				keys(IDX_BOTTOM)<='1';
			elsif kbrd_data_out(5)='1' then  -- key 5
				RESET_CELL <= '0';
				keys(IDX_ENTER) <= '1';
			elsif kbrd_data_out(12)='1' then -- key A, reset
	          	init_active   <= "1101110001000001000111011";
	          	init_selected <= "0000000000001000000000000";
				RESET_CELL <= '1';
			elsif kbrd_data_out(13)='1' then -- key B, reset
	          	init_active   <= "0000000100011100010000000";
	          	init_selected <= "0000000000001000000000000";
				RESET_CELL <= '1';
			elsif kbrd_data_out(14)='1' then -- key C, reset
	          	init_active   <= "1000110001100011000110001";
	          	init_selected <= "0000000000001000000000000";
				RESET_CELL <= '1';
			elsif kbrd_data_out(15)='1' then -- key D, reset
	          	init_active   <= "0000000000000000000000000";
	          	init_selected <= "0000000000001000000000000";
				RESET_CELL <= '1';
			end if;

		end if;
	  end if;
	end process;

	-- GRAPHIC PROCESS
	process (CLK)
	begin  
		
		--RESET_CELL <= '0';
		if rising_edge(CLK) then
			
			-- generovani ctvercu a zmena jejich barev podle podminek

			if (row(11 downto 6) = "000000001") and (col(11 downto 6) = "000000010") then
				if (ACTIVE_CELL(0) = '1' and SELECTED_CELL(0) = '1') then color <= sel_light; 
				elsif (ACTIVE_CELL(0) = '0' and SELECTED_CELL(0) = '1') then color <= sel_dark; 
				elsif (ACTIVE_CELL(0) = '1' and SELECTED_CELL(0) = '0') then color <= light; 
				else color <= dark;
				end if;
			elsif (row(11 downto 6) = "000000001") and (col(11 downto 6) = "000000011") then
				if (ACTIVE_CELL(1) = '1' and SELECTED_CELL(1) = '1') then color <= sel_light; 
				elsif (ACTIVE_CELL(1) = '0' and SELECTED_CELL(1) = '1') then color <= sel_dark; 
				elsif (ACTIVE_CELL(1) = '1' and SELECTED_CELL(1) = '0') then color <= light; 
				else color <= dark;
				end if;
			elsif (row(11 downto 6) = "000000001") and (col(11 downto 6) = "000000100") then
				if (ACTIVE_CELL(2) = '1' and SELECTED_CELL(2) = '1') then color <= sel_light; 
				elsif (ACTIVE_CELL(2) = '0' and SELECTED_CELL(2) = '1') then color <= sel_dark; 
				elsif (ACTIVE_CELL(2) = '1' and SELECTED_CELL(2) = '0') then color <= light; 
				else color <= dark;
				end if;
			elsif (row(11 downto 6) = "000000001") and (col(11 downto 6) = "000000101") then
				if (ACTIVE_CELL(3) = '1' and SELECTED_CELL(3) = '1') then color <= sel_light; 
				elsif (ACTIVE_CELL(3) = '0' and SELECTED_CELL(3) = '1') then color <= sel_dark; 
				elsif (ACTIVE_CELL(3) = '1' and SELECTED_CELL(3) = '0') then color <= light; 
				else color <= dark;
				end if;
			elsif (row(11 downto 6) = "000000001") and (col(11 downto 6) = "000000110") then -- 5
				if (ACTIVE_CELL(4) = '1' and SELECTED_CELL(4) = '1') then color <= sel_light; 
				elsif (ACTIVE_CELL(4) = '0' and SELECTED_CELL(4) = '1') then color <= sel_dark; 
				elsif (ACTIVE_CELL(4) = '1' and SELECTED_CELL(4) = '0') then color <= light; 
				else color <= dark;
				end if;
			elsif (row(11 downto 6) = "000000010") and (col(11 downto 6) = "000000010") then
				if (ACTIVE_CELL(5) = '1' and SELECTED_CELL(5) = '1') then color <= sel_light; 
				elsif (ACTIVE_CELL(5) = '0' and SELECTED_CELL(5) = '1') then color <= sel_dark; 
				elsif (ACTIVE_CELL(5) = '1' and SELECTED_CELL(5) = '0') then color <= light; 
				else color <= dark;
				end if;
			elsif (row(11 downto 6) = "000000010") and (col(11 downto 6) = "000000011") then
				if (ACTIVE_CELL(6) = '1' and SELECTED_CELL(6) = '1') then color <= sel_light; 
				elsif (ACTIVE_CELL(6) = '0' and SELECTED_CELL(6) = '1') then color <= sel_dark; 
				elsif (ACTIVE_CELL(6) = '1' and SELECTED_CELL(6) = '0') then color <= light; 
				else color <= dark;
				end if;
			elsif (row(11 downto 6) = "000000010") and (col(11 downto 6) = "000000100") then
				if (ACTIVE_CELL(7) = '1' and SELECTED_CELL(7) = '1') then color <= sel_light; 
				elsif (ACTIVE_CELL(7) = '0' and SELECTED_CELL(7) = '1') then color <= sel_dark; 
				elsif (ACTIVE_CELL(7) = '1' and SELECTED_CELL(7) = '0') then color <= light; 
				else color <= dark;
				end if;
			elsif (row(11 downto 6) = "000000010") and (col(11 downto 6) = "000000101") then
				if (ACTIVE_CELL(8) = '1' and SELECTED_CELL(8) = '1') then color <= sel_light; 
				elsif (ACTIVE_CELL(8) = '0' and SELECTED_CELL(8) = '1') then color <= sel_dark; 
				elsif (ACTIVE_CELL(8) = '1' and SELECTED_CELL(8) = '0') then color <= light; 
				else color <= dark;
				end if;
			elsif (row(11 downto 6) = "000000010") and (col(11 downto 6) = "000000110") then -- 10
				if (ACTIVE_CELL(9) = '1' and SELECTED_CELL(9) = '1') then color <= sel_light; 
				elsif (ACTIVE_CELL(9) = '0' and SELECTED_CELL(9) = '1') then color <= sel_dark; 
				elsif (ACTIVE_CELL(9) = '1' and SELECTED_CELL(9) = '0') then color <= light; 
				else color <= dark;
				end if;
			elsif (row(11 downto 6) = "000000011") and (col(11 downto 6) = "000000010") then
				if (ACTIVE_CELL(10) = '1' and SELECTED_CELL(10) = '1') then color <= sel_light; 
				elsif (ACTIVE_CELL(10) = '0' and SELECTED_CELL(10) = '1') then color <= sel_dark; 
				elsif (ACTIVE_CELL(10) = '1' and SELECTED_CELL(10) = '0') then color <= light; 
				else color <= dark;
				end if;
			elsif (row(11 downto 6) = "000000011") and (col(11 downto 6) = "000000011") then
				if (ACTIVE_CELL(11) = '1' and SELECTED_CELL(11) = '1') then color <= sel_light; 
				elsif (ACTIVE_CELL(11) = '0' and SELECTED_CELL(11) = '1') then color <= sel_dark; 
				elsif (ACTIVE_CELL(11) = '1' and SELECTED_CELL(11) = '0') then color <= light; 
				else color <= dark;
				end if;
			elsif (row(11 downto 6) = "000000011") and (col(11 downto 6) = "000000100") then
				if (ACTIVE_CELL(12) = '1' and SELECTED_CELL(12) = '1') then color <= sel_light; 
				elsif (ACTIVE_CELL(12) = '0' and SELECTED_CELL(12) = '1') then color <= sel_dark; 
				elsif (ACTIVE_CELL(12) = '1' and SELECTED_CELL(12) = '0') then color <= light; 
				else color <= dark;
				end if;
			elsif (row(11 downto 6) = "000000011") and (col(11 downto 6) = "000000101") then
			  if (ACTIVE_CELL(13) = '1' and SELECTED_CELL(13) = '1') then color <= sel_light; 
				elsif (ACTIVE_CELL(13) = '0' and SELECTED_CELL(13) = '1') then color <= sel_dark; 
				elsif (ACTIVE_CELL(13) = '1' and SELECTED_CELL(13) = '0') then color <= light; 
				else color <= dark;
				end if;
			elsif (row(11 downto 6) = "000000011") and (col(11 downto 6) = "000000110") then -- 15
				if (ACTIVE_CELL(14) = '1' and SELECTED_CELL(14) = '1') then color <= sel_light; 
				elsif (ACTIVE_CELL(14) = '0' and SELECTED_CELL(14) = '1') then color <= sel_dark; 
				elsif (ACTIVE_CELL(14) = '1' and SELECTED_CELL(14) = '0') then color <= light; 
				else color <= dark;
				end if;
			elsif (row(11 downto 6) = "000000100") and (col(11 downto 6) = "000000010") then
				if (ACTIVE_CELL(15) = '1' and SELECTED_CELL(15) = '1') then	color <= sel_light; 
				elsif (ACTIVE_CELL(15) = '0' and SELECTED_CELL(15) = '1') then color <= sel_dark; 
				elsif (ACTIVE_CELL(15) = '1' and SELECTED_CELL(15) = '0') then color <= light; 
				else color <= dark;
				end if;
			elsif (row(11 downto 6) = "000000100") and (col(11 downto 6) = "000000011") then
				if (ACTIVE_CELL(16) = '1' and SELECTED_CELL(16) = '1') then color <= sel_light; 
				elsif (ACTIVE_CELL(16) = '0' and SELECTED_CELL(16) = '1') then color <= sel_dark; 
				elsif (ACTIVE_CELL(16) = '1' and SELECTED_CELL(16) = '0') then color <= light; 
				else color <= dark;
				end if;
			elsif (row(11 downto 6) = "000000100") and (col(11 downto 6) = "000000100") then
				if (ACTIVE_CELL(17) = '1' and SELECTED_CELL(17) = '1') then color <= sel_light; 
				elsif (ACTIVE_CELL(17) = '0' and SELECTED_CELL(17) = '1') then color <= sel_dark; 
				elsif (ACTIVE_CELL(17) = '1' and SELECTED_CELL(17) = '0') then color <= light; 
				else color <= dark;
				end if;
			elsif (row(11 downto 6) = "000000100") and (col(11 downto 6) = "000000101") then
				if (ACTIVE_CELL(18) = '1' and SELECTED_CELL(18) = '1') then color <= sel_light; 
				elsif (ACTIVE_CELL(18) = '0' and SELECTED_CELL(18) = '1') then color <= sel_dark; 
				elsif (ACTIVE_CELL(18) = '1' and SELECTED_CELL(18) = '0') then color <= light; 
				else color <= dark;
				end if;
			elsif (row(11 downto 6) = "000000100") and (col(11 downto 6) = "000000110") then -- 20
				if (ACTIVE_CELL(19) = '1' and SELECTED_CELL(19) = '1') then color <= sel_light; 
				elsif (ACTIVE_CELL(19) = '0' and SELECTED_CELL(19) = '1') then color <= sel_dark; 
				elsif (ACTIVE_CELL(19) = '1' and SELECTED_CELL(19) = '0') then color <= light; 
				else color <= dark;
				end if;
			elsif (row(11 downto 6) = "000000101") and (col(11 downto 6) = "000000010") then
				if (ACTIVE_CELL(20) = '1' and SELECTED_CELL(20) = '1') then color <= sel_light; 
				elsif (ACTIVE_CELL(20) = '0' and SELECTED_CELL(20) = '1') then color <= sel_dark; 
				elsif (ACTIVE_CELL(20) = '1' and SELECTED_CELL(20) = '0') then color <= light; 
				else color <= dark;
				end if;	
			elsif (row(11 downto 6) = "000000101") and (col(11 downto 6) = "000000011") then
				if (ACTIVE_CELL(21) = '1' and SELECTED_CELL(21) = '1') then color <= sel_light; 
				elsif (ACTIVE_CELL(21) = '0' and SELECTED_CELL(21) = '1') then color <= sel_dark; 
				elsif (ACTIVE_CELL(21) = '1' and SELECTED_CELL(21) = '0') then color <= light; 
				else color <= dark;
				end if;
			elsif (row(11 downto 6) = "000000101") and (col(11 downto 6) = "000000100") then
				if (ACTIVE_CELL(22) = '1' and SELECTED_CELL(22) = '1') then color <= sel_light; 
				elsif (ACTIVE_CELL(22) = '0' and SELECTED_CELL(22) = '1') then color <= sel_dark; 
				elsif (ACTIVE_CELL(22) = '1' and SELECTED_CELL(22) = '0') then color <= light; 
				else color <= dark;
				end if;
			
			elsif (row(11 downto 6) = "000000101") and (col(11 downto 6) = "000000101") then
				if (ACTIVE_CELL(23) = '1' and SELECTED_CELL(23) = '1') then color <= sel_light; 
				elsif (ACTIVE_CELL(23) = '0' and SELECTED_CELL(23) = '1') then color <= sel_dark; 
				elsif (ACTIVE_CELL(23) = '1' and SELECTED_CELL(23) = '0') then color <= light; 
				else color <= dark;
				end if;
			elsif (row(11 downto 6) = "000000101") and (col(11 downto 6) = "000000110") then -- 25
				if (ACTIVE_CELL(24) = '1' and SELECTED_CELL(24) = '1') then color <= sel_light; 
				elsif (ACTIVE_CELL(24) = '0' and SELECTED_CELL(24) = '1') then color <= sel_dark; 
				elsif (ACTIVE_CELL(24) = '1' and SELECTED_CELL(24) = '0') then color <= light; 
				else color <= dark;
				end if; 
			else
				color <=  "000000000";
			end if;
	 
			-- generoveni pruhu oddelujici jednotlive ctverce
			if (row=128 or row=192 or row=255 or row=320 or col=192 or col=255 or col=320 or col=384) then
				color <= "000000000";  
			end if;

			-- ramecek
			if (col(11 downto 4) = "000000000") or (col(11 downto 4) = "000100011") or
			   (row(11 downto 4) = "000000000") or (row(11 downto 4) = "000011101")then 
				color <=  "000111000";
			end if;

			-- posledni cerny pruh
			if (col(11 downto 6) = "000001001") then
				color <=  "000000000";
			end if;
			
		end if;				
	end process;   
end main;