-- file:	maskpack.vhd
-- author:	CYRIL URBAN
-- date:	2015-03-02


library IEEE;
use IEEE.STD_LOGIC_1164.all;

package maskpack is

 type mask_t  is
  record
    top        : std_logic;
	 left       : std_logic;
	 right      : std_logic;
	 bottom     : std_logic;
 end record;

 
 function getmask(x,y : natural; COLUMNS, ROWS : natural) return mask_t;
 
end maskpack;

package body maskpack is

	function getmask(x,y : natural; COLUMNS, ROWS : natural) return mask_t is


	variable position : mask_t;

	
		begin
								
			if (x = 0) then
				position.left:='0';
			else
				position.left:='1';
			end if;
	 
			if (x=(COLUMNS-1)) then
				position.right:='0';
			else
				position.right:='1';
			end if;
	 
			if (y = 0) then
				position.top:='0';
			else
				position.top:='1';
			end if;
	 
			if (y=(ROWS-1)) then
				position.bottom:='0';
			else
				position.bottom:='1';
			end if;
			
			return position;
	  end;

end maskpack;