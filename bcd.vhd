-- file:	bcd.vhd
-- author:	CYRIL URBAN
-- date:	2015-03-31
-- brief:	BCD counter

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;

entity bcd is
  port(
    clk: in std_logic;
	 reset: in std_logic;
    NUMBER1: buffer std_logic_vector(3 downto 0) := "0000"; 
	 NUMBER2: buffer std_logic_vector(3 downto 0) := "0000";
	 NUMBER3: buffer std_logic_vector(3 downto 0) := "0000"
	 );
end bcd;

architecture Behavioral of bcd is

begin
	bcd: process( clk )
	begin
		
		if (reset = '1') then
			NUMBER1 <= "0000";
			NUMBER2 <= "0000";
			NUMBER3 <= "0000";	
		
		elsif (clk'event and clk = '1') then
		
				if (NUMBER3 = "0001") then
					NUMBER3 <= "0000";
				else
					if (NUMBER1 = "1001") then
						NUMBER1 <= "0000";
					
						if (NUMBER2 = "1001") then
							NUMBER2 <= "0000";
							NUMBER3 <= "0001";								
						else 
							NUMBER2 <= NUMBER2 + 1;
						end if;
					
					else
							NUMBER1 <= NUMBER1 + 1;				
					end if;
					
				end if;
				
		end if;

	end process;

end Behavioral;
